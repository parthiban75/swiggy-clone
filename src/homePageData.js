export const homePageData = {
    "statusCode": 0,
    "data": {
      "cacheExpiryTime": 320,
      "pages": 92,
      "pageIndex": 0,
      "scrubber": 0,
      "configs": {
        "ribbons": {
          "PREORDER": {
            "type": "PREORDER",
            "text": "Preorder",
            "textColor": "#ffffff",
            "imageId": "sfefefefeegeg",
            "topBackgroundColor": "#d53d4c",
            "bottomBackgroundColor": "#af2330",
            "priority": 3
          },
          "EXCLUSIVE": {
            "type": "EXCLUSIVE",
            "text": "Exclusive",
            "textColor": "#ffffff",
            "imageId": "sfefefefeegeg",
            "topBackgroundColor": "#fa4a5b",
            "bottomBackgroundColor": "#d12a3b",
            "priority": 2
          },
          "NEW": {
            "type": "NEW",
            "text": "Newly Added",
            "textColor": "#ffffff",
            "imageId": "sfefefefeegeg",
            "topBackgroundColor": "#d53d4c",
            "bottomBackgroundColor": "#af2330",
            "priority": 4
          },
          "REPEAT": {
            "type": "REPEAT",
            "text": "Repeat",
            "textColor": "#ffffff",
            "imageId": "sfefefefeegeg",
            "topBackgroundColor": "#D53D4C",
            "bottomBackgroundColor": "#B02331",
            "priority": 6
          },
          "CLOUD": {
            "type": "CLOUD",
            "text": "Daily Menus",
            "textColor": "#ffffff",
            "imageId": "sfefefefeegeg",
            "topBackgroundColor": "#fa4a5b",
            "bottomBackgroundColor": "#d12a3b",
            "priority": 5
          },
          "PREMIUM": {
            "type": "PREMIUM",
            "text": "Premium",
            "textColor": "#ffffff",
            "imageId": "sfefefefeegeg",
            "topBackgroundColor": "#8a584b",
            "bottomBackgroundColor": "#583229",
            "priority": 7
          },
          "PROMOTED": {
            "type": "PROMOTED",
            "text": "Promoted",
            "textColor": "#ffffff",
            "imageId": "sfefefefeegeg",
            "topBackgroundColor": "#3a3c41",
            "bottomBackgroundColor": "#1e2023",
            "priority": 1
          }
        },
        "croutons": {
          "RAIN": {
            "type": "Crouton",
            "bgColor": "#282C3F",
            "icon": "surge_listing_piuzrv",
            "textColor": "#ffffff",
            "title": "Bad Weather",
            "message": "${amount} extra levied on some restaurants"
          },
          "RAIN_DEFAULT": {
            "type": "Crouton",
            "bgColor": "#282C3F",
            "icon": "surge_listing_piuzrv",
            "textColor": "#ffffff",
            "title": "Bad Weather",
            "message": "Your orders may be delayed by 5-10 mins"
          },
          "SERVICEABLE_WITH_BANNER_RAIN": {
            "type": "Crouton",
            "bgColor": "#282C3F",
            "icon": "rain_crouton_4x",
            "textColor": "#ffffff",
            "title": "Bad Weather",
            "message": "Few restaurants unserviceable due to rains"
          },
          "RAIN_LOW": {
            "type": "Crouton",
            "bgColor": "#282C3F",
            "icon": "surge_listing_piuzrv",
            "textColor": "#ffffff",
            "title": "Bad Weather",
            "message": "Your orders may be delayed by 5-10 mins"
          },
          "RAIN_HIGH": {
            "type": "Crouton",
            "bgColor": "#282C3F",
            "icon": "surge_listing_piuzrv",
            "textColor": "#ffffff",
            "title": "Bad Weather",
            "message": "Your orders may be delayed by 5-10 mins"
          },
          "SPECIAL": {
            "type": "Crouton",
            "bgColor": "#282C3F",
            "icon": "surge_listing_piuzrv",
            "textColor": "#ffffff",
            "title": "High Demand",
            "message": "Surge fee of Rs ${amount} may be applicable"
          }
        }
      },
      "cards": [
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "531344",
              "name": "N R Donne Biriyani",
              "uuid": "e9a293a7-ba0b-4e70-91ef-b1c0a9fa42c6",
              "city": "1",
              "area": "Jayanagar",
              "totalRatingsString": "500+ ratings",
              "cloudinaryImageId": "qfkjs9o02js0byfp9nr7",
              "cuisines": ["Biryani", "South Indian"],
              "tags": [],
              "costForTwo": 25000,
              "costForTwoString": "₹250 FOR TWO",
              "deliveryTime": 25,
              "minDeliveryTime": 25,
              "maxDeliveryTime": 25,
              "slaString": "25 MINS",
              "lastMileTravel": 3.700000047683716,
              "slugs": {
                "restaurant": "n-r-donne-biriyani-jayanagar-jayanagar",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "NO 5TH CROSS JAYANAGAR , Jayanagar ,B.B.M.P South, Karnataka-560011",
              "locality": "1st Block",
              "parentId": 319722,
              "unserviceable": false,
              "veg": false,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "aggregatedDiscountInfo": {
                "header": "50% off",
                "shortDescriptionList": [
                  {
                    "meta": "50% off | Use GUILTFREE",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "50% off up to ₹120 | Use code GUILTFREE",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "aggregatedDiscountInfoV2": {
                "header": "50% OFF",
                "shortDescriptionList": [
                  {
                    "meta": "Use GUILTFREE",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "50% off up to ₹120 | Use code GUILTFREE",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "ribbon": [{ "type": "PROMOTED" }],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "cid=5833645~p=1~eid=00000186-0c79-5598-2f78-c63e00c10116",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "3.7 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "531344",
                "deliveryTime": 25,
                "minDeliveryTime": 25,
                "maxDeliveryTime": 25,
                "lastMileTravel": 3.700000047683716,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": true,
              "avgRating": "4.2",
              "totalRatings": 500,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "655",
              "name": "Meghana Foods",
              "uuid": "3a8279e3-999c-4612-874d-812a47e84ce0",
              "city": "1",
              "area": "Jayanagar",
              "totalRatingsString": "1000+ ratings",
              "cloudinaryImageId": "x4uyxvihmg8qa3pddkgf",
              "cuisines": [
                "Biryani",
                "Andhra",
                "South Indian",
                "North Indian",
                "Chinese",
                "Seafood"
              ],
              "tags": [],
              "costForTwo": 50000,
              "costForTwoString": "₹500 FOR TWO",
              "deliveryTime": 29,
              "minDeliveryTime": 29,
              "maxDeliveryTime": 29,
              "slaString": "29 MINS",
              "lastMileTravel": 4.599999904632568,
              "slugs": {
                "restaurant": "meghana-foods-4th-block-jayanagar",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "1st Floor,Plot No.52, 33rd Cross Road, Jayanagar 4th Block",
              "locality": "4th Block",
              "parentId": 635,
              "unserviceable": false,
              "veg": false,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "4.5 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "655",
                "deliveryTime": 29,
                "minDeliveryTime": 29,
                "maxDeliveryTime": 29,
                "lastMileTravel": 4.599999904632568,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": false,
              "avgRating": "4.3",
              "totalRatings": 1000,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "121603",
              "name": "Kannur Food Point",
              "uuid": "51983905-e698-4e31-b0d7-e376eca56320",
              "city": "1",
              "area": "Tavarekere",
              "totalRatingsString": "5000+ ratings",
              "cloudinaryImageId": "bmwn4n4bn6n1tcpc8x2h",
              "cuisines": ["Kerala", "Chinese"],
              "tags": [],
              "costForTwo": 30000,
              "costForTwoString": "₹300 FOR TWO",
              "deliveryTime": 33,
              "minDeliveryTime": 33,
              "maxDeliveryTime": 33,
              "slaString": "33 MINS",
              "lastMileTravel": 5.699999809265137,
              "slugs": {
                "restaurant": "kannur-food-point-btm",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "6/21,9TH CROSS ,1ST MAIN, VENKATESHWARA LAYOUT,SG PALYA, BENGALURU, - 560093",
              "locality": "SG Palya",
              "parentId": 20974,
              "unserviceable": false,
              "veg": false,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "aggregatedDiscountInfo": {
                "header": "50% off",
                "shortDescriptionList": [
                  {
                    "meta": "50% off on all orders",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "50% off on all orders",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "aggregatedDiscountInfoV2": {
                "header": "50% OFF",
                "shortDescriptionList": [
                  {
                    "meta": "",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "50% off on all orders",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "5.6 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "121603",
                "deliveryTime": 33,
                "minDeliveryTime": 33,
                "maxDeliveryTime": 33,
                "lastMileTravel": 5.699999809265137,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": false,
              "avgRating": "3.9",
              "totalRatings": 5000,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "369642",
              "name": "Basaveshwar Khanavali",
              "uuid": "2c25bf16-6bed-4f6a-aaf7-c8654469d76f",
              "city": "1",
              "area": "Jayanagar",
              "totalRatingsString": "100+ ratings",
              "cloudinaryImageId": "ojmkakt4nm1rvnqr8ufr",
              "cuisines": ["Indian"],
              "tags": [],
              "costForTwo": 30000,
              "costForTwoString": "₹300 FOR TWO",
              "deliveryTime": 28,
              "minDeliveryTime": 28,
              "maxDeliveryTime": 28,
              "slaString": "28 MINS",
              "lastMileTravel": 3.799999952316284,
              "slugs": {
                "restaurant": "basaveshwar-khanavali-jayanagar-jayanagar",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "NO, 228/15, 1ST FLOOR 9TH MAIN ROAD 3RD BLOCK JAYANAGAR BENGALURU,560011",
              "locality": "3rd Block",
              "parentId": 42124,
              "unserviceable": false,
              "veg": true,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "aggregatedDiscountInfo": {
                "header": "40% off",
                "shortDescriptionList": [
                  {
                    "meta": "40% off | Use TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "40% off up to ₹80 | Use code TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "aggregatedDiscountInfoV2": {
                "header": "40% OFF",
                "shortDescriptionList": [
                  {
                    "meta": "Use TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "40% off up to ₹80 | Use code TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "ribbon": [{ "type": "PROMOTED" }],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "cid=5838099~p=4~eid=00000186-0c79-5598-2f78-c63f00c10448",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "3.7 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "369642",
                "deliveryTime": 28,
                "minDeliveryTime": 28,
                "maxDeliveryTime": 28,
                "lastMileTravel": 3.799999952316284,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": true,
              "avgRating": "4.5",
              "totalRatings": 100,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "583",
              "name": "Hotel Empire",
              "uuid": "9cae76f3-4988-4733-a1bb-db1af22eb891",
              "city": "1",
              "area": "Jayanagar",
              "totalRatingsString": "1000+ ratings",
              "cloudinaryImageId": "r4mw2xp0djz0pscwmiop",
              "cuisines": ["North Indian", "Kebabs", "Biryani"],
              "tags": [],
              "costForTwo": 45000,
              "costForTwoString": "₹450 FOR TWO",
              "deliveryTime": 28,
              "minDeliveryTime": 28,
              "maxDeliveryTime": 28,
              "slaString": "28 MINS",
              "lastMileTravel": 3.700000047683716,
              "slugs": {
                "restaurant": "hotel-empire-3rd-block-jayanagar",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "4, 21st C Cross Road, Opposite NMKRV College, 3rd Block,Jayanagar",
              "locality": "Jayanagar",
              "parentId": 475,
              "unserviceable": false,
              "veg": false,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "aggregatedDiscountInfo": {
                "header": "10% off",
                "shortDescriptionList": [
                  {
                    "meta": "10% off | Use TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "10% off up to ₹40 | Use code TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "aggregatedDiscountInfoV2": {
                "header": "10% OFF",
                "shortDescriptionList": [
                  {
                    "meta": "Use TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "10% off up to ₹40 | Use code TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "ribbon": [{ "type": "PROMOTED" }],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "cid=5853660~p=7~eid=00000186-0c79-5598-2f78-c64000c10718",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "3.7 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "583",
                "deliveryTime": 28,
                "minDeliveryTime": 28,
                "maxDeliveryTime": 28,
                "lastMileTravel": 3.700000047683716,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": true,
              "avgRating": "4.2",
              "totalRatings": 1000,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "56428",
              "name": "Puliyogare Point",
              "uuid": "49970568-119d-436c-9dad-042d9c07c7ff",
              "city": "1",
              "area": "Basavanagudi",
              "totalRatingsString": "1000+ ratings",
              "cloudinaryImageId": "wet9zrjusmdtkk16jlrz",
              "cuisines": ["South Indian"],
              "tags": [],
              "costForTwo": 15000,
              "costForTwoString": "₹150 FOR TWO",
              "deliveryTime": 24,
              "minDeliveryTime": 20,
              "maxDeliveryTime": 30,
              "slaString": "20-30 MINS",
              "lastMileTravel": 2.5,
              "slugs": {
                "restaurant": "puliyogare-point-basavanagudi-basavanagudi",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "No 81, East Anjaneya Temple Street, N.R Colony, Basavanagudi",
              "locality": "N.R Colony",
              "parentId": 8409,
              "unserviceable": false,
              "veg": true,
              "select": false,
              "favorite": true,
              "tradeCampaignHeaders": [],
              "aggregatedDiscountInfo": {
                "header": "10% off",
                "shortDescriptionList": [
                  {
                    "meta": "10% off | Use MISSEDYOU",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "10% off up to ₹40 | Use code MISSEDYOU",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "aggregatedDiscountInfoV2": {
                "header": "10% OFF",
                "shortDescriptionList": [
                  {
                    "meta": "Use MISSEDYOU",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "10% off up to ₹40 | Use code MISSEDYOU",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "2.5 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "56428",
                "deliveryTime": 24,
                "minDeliveryTime": 20,
                "maxDeliveryTime": 30,
                "lastMileTravel": 2.5,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": false,
              "avgRating": "4.5",
              "totalRatings": 1000,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "1050",
              "name": "Nagarjuna Chimney",
              "uuid": "d4e2272f-90f2-4dd0-9bec-258eee0e692e",
              "city": "1",
              "area": "Jaya Nagar",
              "totalRatingsString": "500+ ratings",
              "cloudinaryImageId": "hirj3b66f3uphbdinhev",
              "cuisines": [
                "Seafood",
                "Andhra",
                "Biryani",
                "South Indian",
                "Desserts",
                "Beverages"
              ],
              "tags": [],
              "costForTwo": 80000,
              "costForTwoString": "₹800 FOR TWO",
              "deliveryTime": 32,
              "minDeliveryTime": 32,
              "maxDeliveryTime": 32,
              "slaString": "32 MINS",
              "lastMileTravel": 4.599999904632568,
              "slugs": {
                "restaurant": "nagarjuna-chimney-3rd-block-jayanagar",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "21/22, 22nd Cross, 3rd Block, Jayanagar",
              "locality": "3rd Block",
              "parentId": 143166,
              "unserviceable": false,
              "veg": false,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "ribbon": [{ "type": "PROMOTED" }],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "cid=5843200~p=10~eid=00000186-0c79-5598-2f78-c64100c10a6b",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "4.5 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "1050",
                "deliveryTime": 32,
                "minDeliveryTime": 32,
                "maxDeliveryTime": 32,
                "lastMileTravel": 4.599999904632568,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": true,
              "avgRating": "4.3",
              "totalRatings": 500,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "46647",
              "name": "Ayodhya Upachar",
              "uuid": "ce3fa965-bb59-4850-8cf6-eac332f8c674",
              "city": "1",
              "area": "Banashankari",
              "totalRatingsString": "1000+ ratings",
              "cloudinaryImageId": "fyx10gm1sfvbfmrgdtb2",
              "cuisines": [
                "Chinese",
                "Desserts",
                "North Indian",
                "South Indian",
                "Chaat"
              ],
              "tags": [],
              "costForTwo": 20000,
              "costForTwoString": "₹200 FOR TWO",
              "deliveryTime": 34,
              "minDeliveryTime": 34,
              "maxDeliveryTime": 34,
              "slaString": "34 MINS",
              "lastMileTravel": 6,
              "slugs": {
                "restaurant": "ayodhya-upachar-banashankari-banashankari",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "#199. Outer ring road.janatha bazaar bus stop .Kathriguppe.Bangalore",
              "locality": "Kathriguppe",
              "parentId": 370539,
              "unserviceable": false,
              "veg": true,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "6 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "46647",
                "deliveryTime": 34,
                "minDeliveryTime": 34,
                "maxDeliveryTime": 34,
                "lastMileTravel": 6,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": false,
              "avgRating": "4.4",
              "totalRatings": 1000,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "33083",
              "name": "Chikpet Donne Biryani House - Vijayanagar",
              "uuid": "f5611786-08ce-4e16-9144-f0ff9c26a335",
              "city": "1",
              "area": "Vijayanagar",
              "totalRatingsString": "500+ ratings",
              "cloudinaryImageId": "cpbq1uck4icuhdqvuwb0",
              "cuisines": ["Biryani", "South Indian"],
              "tags": [],
              "costForTwo": 30000,
              "costForTwoString": "₹300 FOR TWO",
              "deliveryTime": 24,
              "minDeliveryTime": 24,
              "maxDeliveryTime": 24,
              "slaString": "24 MINS",
              "lastMileTravel": 4.699999809265137,
              "slugs": {
                "restaurant": "donne-biryani-house-vijaynagar-vijayanagar",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "NO 105,Club road,Vijaynagar,Nr mcdonalds,Opp Titan showroom Blr-560040",
              "locality": "Club Road",
              "parentId": 347013,
              "unserviceable": false,
              "veg": false,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "4.6 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "33083",
                "deliveryTime": 24,
                "minDeliveryTime": 24,
                "maxDeliveryTime": 24,
                "lastMileTravel": 4.699999809265137,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": false,
              "avgRating": "4.1",
              "totalRatings": 500,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "484182",
              "name": "California Burrito",
              "uuid": "80c65672-960a-4ae1-93ad-8612e51a4eca",
              "city": "1",
              "area": "Basavanagudi",
              "totalRatingsString": "500+ ratings",
              "cloudinaryImageId": "dgaistubp3t7pd2r2cff",
              "cuisines": [
                "Mexican",
                "American",
                "Salads",
                "Continental",
                "Keto",
                "Healthy Food"
              ],
              "tags": [],
              "costForTwo": 30000,
              "costForTwoString": "₹300 FOR TWO",
              "deliveryTime": 25,
              "minDeliveryTime": 20,
              "maxDeliveryTime": 30,
              "slaString": "20-30 MINS",
              "lastMileTravel": 2,
              "slugs": {
                "restaurant": "california-burrito-basavanagudi-basavanagudi",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "97, Surya Gandhi Bazar, Main Road, Bangalore, Karnataka -560004",
              "locality": "Gandhi Bazaar",
              "parentId": 1252,
              "unserviceable": false,
              "veg": false,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "aggregatedDiscountInfo": {
                "header": "40% off",
                "shortDescriptionList": [
                  {
                    "meta": "40% off | Use TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "40% off up to ₹80 | Use code TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "aggregatedDiscountInfoV2": {
                "header": "40% OFF",
                "shortDescriptionList": [
                  {
                    "meta": "Use TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "40% off up to ₹80 | Use code TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "ribbon": [{ "type": "PROMOTED" }],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "cid=5843472~p=16~eid=00000186-0c79-5598-2f78-c64300c11039",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "2 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "484182",
                "deliveryTime": 25,
                "minDeliveryTime": 20,
                "maxDeliveryTime": 30,
                "lastMileTravel": 2,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": true,
              "avgRating": "4.5",
              "totalRatings": 500,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "55176",
              "name": "Hotel Kadamba Veg",
              "uuid": "7fa082cf-7374-49a7-8ba1-d97e757b4d46",
              "city": "1",
              "area": "Rajajinagar",
              "totalRatingsString": "1000+ ratings",
              "cloudinaryImageId": "ywdpqvhsmaru4uef8szm",
              "cuisines": ["South Indian", "North Indian", "Chinese", "Sweets"],
              "tags": [],
              "costForTwo": 35000,
              "costForTwoString": "₹350 FOR TWO",
              "deliveryTime": 36,
              "minDeliveryTime": 36,
              "maxDeliveryTime": 36,
              "slaString": "36 MINS",
              "lastMileTravel": 6.5,
              "slugs": {
                "restaurant": "hotel-kadamba-veg-rajajinagar-rajajinagar",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "#707, VENKATADRI, Modi Hospital Road, Rajajinagar Bangalore - 560086",
              "locality": "Basaveshwara Nagar",
              "parentId": 99592,
              "unserviceable": false,
              "veg": true,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "aggregatedDiscountInfo": {
                "header": "10% off",
                "shortDescriptionList": [
                  {
                    "meta": "10% off | Use PARTY",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "10% off | Use code PARTY",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "aggregatedDiscountInfoV2": {
                "header": "10% OFF",
                "shortDescriptionList": [
                  {
                    "meta": "Use PARTY",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "10% off | Use code PARTY",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "6.5 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "55176",
                "deliveryTime": 36,
                "minDeliveryTime": 36,
                "maxDeliveryTime": 36,
                "lastMileTravel": 6.5,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": false,
              "avgRating": "4.3",
              "totalRatings": 1000,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "47493",
              "name": "Hari Super Sandwich (3rd Block)",
              "uuid": "97798836-f15a-4566-bdeb-7e4d1b0eac20",
              "city": "1",
              "area": "Jayanagar",
              "totalRatingsString": "1000+ ratings",
              "cloudinaryImageId": "ml1ituszk5qurjia3r9h",
              "cuisines": ["Chaat", "Snacks", "Pizzas", "North Indian", "Indian"],
              "tags": [],
              "costForTwo": 10000,
              "costForTwoString": "₹100 FOR TWO",
              "deliveryTime": 28,
              "minDeliveryTime": 28,
              "maxDeliveryTime": 28,
              "slaString": "28 MINS",
              "lastMileTravel": 3.700000047683716,
              "slugs": {
                "restaurant": "hari-super-sandwich-jayanagar-jayanagar",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "No 177/A 22nd cross 3rd block Jayanagar Bangalore 560011",
              "locality": "3rd Block",
              "parentId": 352495,
              "unserviceable": false,
              "veg": true,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "aggregatedDiscountInfo": {
                "header": "FLAT100 off",
                "shortDescriptionList": [
                  {
                    "meta": "FLAT100 off | Use FLATDEAL",
                    "discountType": "Flat",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "FLAT100 off | Use FLATDEAL",
                    "discountType": "Flat",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "aggregatedDiscountInfoV2": {
                "header": "₹100 OFF",
                "shortDescriptionList": [
                  {
                    "meta": "Use FLATDEAL",
                    "discountType": "Flat",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "FLAT100 off | Use FLATDEAL",
                    "discountType": "Flat",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "3.7 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "47493",
                "deliveryTime": 28,
                "minDeliveryTime": 28,
                "maxDeliveryTime": 28,
                "lastMileTravel": 3.700000047683716,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": false,
              "avgRating": "4.0",
              "totalRatings": 1000,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "33866",
              "name": "Karnataka biryani paradise",
              "uuid": "d47d26d3-d218-4e6d-aed7-d62856cd0953",
              "city": "1",
              "area": "Nagarbhavi",
              "totalRatingsString": "100+ ratings",
              "cloudinaryImageId": "vdn0gihrrqfi5jymkrpb",
              "cuisines": ["Biryani", "Chinese"],
              "tags": [],
              "costForTwo": 20000,
              "costForTwoString": "₹200 FOR TWO",
              "deliveryTime": 31,
              "minDeliveryTime": 31,
              "maxDeliveryTime": 31,
              "slaString": "31 MINS",
              "lastMileTravel": 6.099999904632568,
              "slugs": {
                "restaurant": "karnataka-biryani-paradise-nagarbhavi-vijayanagar",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "Skyline City Appt Road Chandra Layout",
              "locality": "Chanra Layout",
              "parentId": 115220,
              "unserviceable": false,
              "veg": false,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "6 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "33866",
                "deliveryTime": 31,
                "minDeliveryTime": 31,
                "maxDeliveryTime": 31,
                "lastMileTravel": 6.099999904632568,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": false,
              "avgRating": "3.6",
              "totalRatings": 100,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "222046",
              "name": "Chikpet Donne Biryani House Palace Guttahalli",
              "uuid": "57fca181-08ce-4d56-a737-d45bef81c5ac",
              "city": "1",
              "area": "Seshadripuram",
              "totalRatingsString": "500+ ratings",
              "cloudinaryImageId": "ckpftg1mcfxnlm392vvj",
              "cuisines": ["Biryani", "South Indian"],
              "tags": [],
              "costForTwo": 30000,
              "costForTwoString": "₹300 FOR TWO",
              "deliveryTime": 27,
              "minDeliveryTime": 27,
              "maxDeliveryTime": 27,
              "slaString": "27 MINS",
              "lastMileTravel": 4.900000095367432,
              "slugs": {
                "restaurant": "chikpet-donne-biryani-house-malleshwaram-malleshwaram",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "119,1st Main Road,Palace Guttahalli,Opp North WInd Bar,Shehsadripuram,Bangalore-20",
              "locality": "Palace Guttahalli",
              "parentId": 353241,
              "unserviceable": false,
              "veg": false,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "4.9 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "222046",
                "deliveryTime": 27,
                "minDeliveryTime": 27,
                "maxDeliveryTime": 27,
                "lastMileTravel": 4.900000095367432,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": false,
              "avgRating": "4.0",
              "totalRatings": 500,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "210945",
              "name": "Royal Restaurant",
              "uuid": "9b33bca4-65e6-4a26-af3e-f47c4476ed4d",
              "city": "1",
              "area": "Shivajinagar",
              "totalRatingsString": "500+ ratings",
              "cloudinaryImageId": "yicweopn4lzcjlqy4jvq",
              "cuisines": ["Chinese", "North Indian", "Tandoor"],
              "tags": [],
              "costForTwo": 30000,
              "costForTwoString": "₹300 FOR TWO",
              "deliveryTime": 36,
              "minDeliveryTime": 36,
              "maxDeliveryTime": 36,
              "slaString": "36 MINS",
              "lastMileTravel": 5,
              "slugs": {
                "restaurant": "royal-restaurant-central-bangalore-central-bangalore",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "Broadway Road Police Station, HKP Road, Sulthangunta, Shivajinagar, Bengaluru, Bangalore Urban, Karnataka, India",
              "locality": "Sulthangunta",
              "parentId": 2896,
              "unserviceable": false,
              "veg": false,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "aggregatedDiscountInfo": {
                "header": "35% off",
                "shortDescriptionList": [
                  {
                    "meta": "35% off on all orders",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "35% off on all orders",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "aggregatedDiscountInfoV2": {
                "header": "35% OFF",
                "shortDescriptionList": [
                  {
                    "meta": "",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "35% off on all orders",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 1,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "5 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "210945",
                "deliveryTime": 36,
                "minDeliveryTime": 36,
                "maxDeliveryTime": 36,
                "lastMileTravel": 5,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "IT_IS_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": false,
              "avgRating": "4.0",
              "totalRatings": 500,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        },
        {
          "cardType": "restaurant",
          "layoutAlignmentType": "VERTICAL",
          "data": {
            "type": "restaurant",
            "data": {
              "type": "F",
              "id": "96527",
              "name": "Vijayalakshmi",
              "uuid": "832cd579-956f-4480-8cf8-42af4fea46a0",
              "city": "1",
              "area": "Basavanagudi",
              "totalRatingsString": "1000+ ratings",
              "cloudinaryImageId": "r94u50lcikbaqulaofdi",
              "cuisines": ["South Indian", "North Indian", "Chinese"],
              "tags": [],
              "costForTwo": 15000,
              "costForTwoString": "₹150 FOR TWO",
              "deliveryTime": 26,
              "minDeliveryTime": 25,
              "maxDeliveryTime": 35,
              "slaString": "25-35 MINS",
              "lastMileTravel": 2.4000000953674316,
              "slugs": {
                "restaurant": "vijayalakshmi-basavanagudi",
                "city": "bangalore"
              },
              "cityState": "1",
              "address": "#37, SHREE COMPLEX, next to dominos pizza, Bull Temple Road, Basavanagudi, Bangalore-560004",
              "locality": "Bull Temple Rd",
              "parentId": 20982,
              "unserviceable": false,
              "veg": true,
              "select": false,
              "favorite": false,
              "tradeCampaignHeaders": [],
              "aggregatedDiscountInfo": {
                "header": "40% off",
                "shortDescriptionList": [
                  {
                    "meta": "40% off | Use TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "40% off up to ₹80 | Use code TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "aggregatedDiscountInfoV2": {
                "header": "40% OFF",
                "shortDescriptionList": [
                  {
                    "meta": "Use TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "descriptionList": [
                  {
                    "meta": "40% off up to ₹80 | Use code TRYNEW",
                    "discountType": "Percentage",
                    "operationType": "RESTAURANT"
                  }
                ],
                "subHeader": "",
                "headerType": 0,
                "superFreedel": ""
              },
              "ribbon": [{ "type": "PROMOTED" }],
              "chain": [],
              "feeDetails": {
                "fees": [],
                "totalFees": 0,
                "message": "",
                "title": "",
                "amount": "",
                "icon": ""
              },
              "availability": {
                "opened": true,
                "nextOpenMessage": "",
                "nextCloseMessage": ""
              },
              "longDistanceEnabled": 0,
              "rainMode": "NONE",
              "thirdPartyAddress": false,
              "thirdPartyVendor": "",
              "adTrackingID": "cid=5867249~p=22~eid=00000186-0c79-5598-2f78-c64500c11661",
              "badges": {
                "imageBased": [],
                "textBased": [],
                "textExtendedBadges": []
              },
              "lastMileTravelString": "2.4 kms",
              "hasSurge": false,
              "sla": {
                "restaurantId": "96527",
                "deliveryTime": 26,
                "minDeliveryTime": 25,
                "maxDeliveryTime": 35,
                "lastMileTravel": 2.4000000953674316,
                "lastMileDistance": 0,
                "serviceability": "SERVICEABLE",
                "rainMode": "NONE",
                "longDistance": "NOT_LONG_DISTANCE",
                "preferentialService": false,
                "iconType": "EMPTY"
              },
              "promoted": true,
              "avgRating": "4.2",
              "totalRatings": 1000,
              "new": false
            },
            "subtype": "basic"
          },
          "parentWidget": false
        }
      ],
      "nextPageFetch": 0
    },
    "tid": "06dccacd-3f7a-4328-9f9e-36cd9bb9508f",
    "sid": "55ra4bd0-8e71-4e8e-b773-87c3cf0bf3d7",
    "deviceId": "6c901d50-0e14-a86a-b80e-dee286d28c54",
    "csrfToken": "TDeAE4DcQcyG-XPAitm7ldE98-TX2Q5UauaMu_NM"
  }
  