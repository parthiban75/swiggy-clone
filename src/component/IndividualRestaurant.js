import React, { Component } from 'react'

class IndividualRestaurant extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        console.log("q", this.props)
        return (
            <div className='individualRestaurant'>
                <div class="card">
                    <img class="card-img-top" className='restaurantImage' src={require('./Images/Offers.webp')} alt="Card image cap"></img>
                        <div class="card-body">
                            <h5 class="card-title">{this.props.name}</h5>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{this.props.cuisine}</li>
                            <li class="list-group-item">{this.props.deliveryTime} MINS</li>
                            <li class="list-group-item">{this.props.offerText}</li>
                            <li class="list-group-item">{this.props.rating} star</li>
                        
                            

                        </ul>
                       
                </div>
            

            </div>
        )
    }
}

export default IndividualRestaurant