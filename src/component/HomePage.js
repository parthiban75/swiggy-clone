import React, { Component } from 'react'
import HomeOurPicksForYou from './HomeOurPicksForYou'
import HomePopularRestaurant from './HomePopularRestaurant'
import SearchBarInHome from './SearchBarInHome'
import "./styles.css"
import Footer from './Footer'

import HomeText from './HomeText'
import Navbar from './Navbar';

class HomePage extends Component {
  
    render() {
        return (
            <div className='mainHomePage'>
                 <div>
      <Navbar></Navbar>

      </div>
                <SearchBarInHome></SearchBarInHome>
                <HomeOurPicksForYou></HomeOurPicksForYou>
                <HomePopularRestaurant></HomePopularRestaurant>
                <HomeText></HomeText>
                <Footer></Footer>
            </div>
        )
    }
}

export default HomePage