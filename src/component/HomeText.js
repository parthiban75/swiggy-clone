import React, { Component } from 'react'

class HomeText extends Component {
    render() {
        return (
            <div className='homePageText'>
                <h3 className='ourPicksHeadline'>About the food culture in Bangalore</h3>
                <h4 className='homeTextIntag'>Reasons to Order Food Online in Bangalore via Swiggy</h4>
                <p className='homeTextIntag' >
                    Bangalore, the IT hub of the country, is the city that is a home to millions of young techies staying far away from their homes.
                    If you are also one of those, who does not have a proper functional kitchen, you can understand the pain of missing out on your favourite food.
                    Imagine one day you are having a hectic day at work, and suddenly you find it’s lunchtime. The rats have already started growling in your stomach
                    but you can’t visit the food court due to your ongoing work. Bummer? But no worries, as Swiggy is here to help you.Just take a pause, order food online in Bangalore, and you can resume work. Swiggy assures you of lightning-fast delivery, and the delivery agents
                    will deliver your food within 45 minutes.
                </p>
                <h4 className='homeTextIntag'>Get your food delivery in Bangalore to experience its food culture</h4>
                <p className='homeTextIntag'>The services of food delivery in Bangalore make it easy for you to taste the delicious dishes that this city has to offer.
                    From authentic pizzas and burgers to crispy masala dosa, idli, vada and droolworthy filter coffee, the city has it all.
                    If you want to start with the famous food of Bangalore, try Ragi Mudde, which are traditional gluten-free balls made with finger
                    millet flour.
                    Biryani lovers must try the Anand Dum Biryani served at Hostoke. Bisi Bele Bath is an authentic vegetarian dish made with a combination
                    of rice, veggies, lentils, and ghee.The combination of akki roti and chutney pudi is comfort food at its best.
                    Don’t forget to gorge on Chow Chow Bath, a popular street food and dessert of Bangalore.
                </p>
            </div>
        )
    }
}

export default HomeText