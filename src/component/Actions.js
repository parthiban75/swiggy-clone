import { GETRESTAURANTDATA  } from "./HomePopularRestaurant"


export function getRestaurantData(restaurantInPageData) {
    return ({
        type: GETRESTAURANTDATA,
        payload: restaurantInPageData
    })
}