import React, { Component } from 'react'
import { homePageData } from '../homePageData'
import { connect } from 'react-redux'
import { getRestaurantData } from './Actions'
import IndividualRestaurant from './IndividualRestaurant'

class HomePopularRestaurant extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        console.log("in")
        this.props.getRestaurantData(homePageData)



        /*let restaurantInPage = restaurantInPageData.data.cards
        let out = restaurantInPage.map((restaurent) => {
            return restaurent.data.data.name

        })
        //console.log(data.data.cards)
        console.log(restaurantInPage)*/
    }


    render() {

        console.log("data from store", this.props)
        //let restaurantArray = this.props.homePageData.Reducer.getRestaurantData.data.cards
        return (
            <div>
                <h3 className='homePopularRestaurantHeading'>Popular Restaurant In And Around Bangalore</h3>
                <div className='homePopularRestaurantContainer'>

                    {
                        (this.props.homePageData.Reducer.getRestaurantData.data.cards).length ?
                            this.props.homePageData.Reducer.getRestaurantData.data.cards.map((restaurant) => {

                                return <IndividualRestaurant key={restaurant.data.data.name} name={restaurant.data.data.name} cuisine={restaurant.data.data.cuisines}
                                    deliveryTime={restaurant.data.data.deliveryTime} offerText={restaurant.data.data.costForTwoString} rating={restaurant.data.data.avgRating}
                                     discount={restaurant.data.data.aggregatedDiscountInfo} />
                            }) : "No restaurant to display"
                    }
                </div>

            </div>

        )
    }
}

//export default HomePopularRestaurant

const mapStateToProps = state => {
    return {
        homePageData: state
    }
}
const mapDispatchToProps = dispatch => {
    return {
        getRestaurantData: (homePageData) => { dispatch(getRestaurantData(homePageData)) },

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePopularRestaurant)

export const GETRESTAURANTDATA = 'GETRESTAURANTDATA '