import Reducer from './Reducer'

const redux = require('redux')
const createStore = redux.createStore
const combineReducers = redux.combineReducers

const rootReducer = combineReducers({
  Reducer : Reducer,

})
const store = createStore(rootReducer,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() )

export default store