import React, { Component } from 'react'

class HomeOurPicksForYou extends Component {
  render() {
    return (
        <div className='ourPicksContainer'>
             <h3 className='ourPicksHeadline'>Our Picks For You</h3>
             <div className='ourPicksOptions'>
                <div className='individualOptions'>                
                <img className='optionsImage' src={require('./Images/Offers.webp')} />
                <h4 className='ourPicksOption'>OFFERS NEAR YOU</h4>
                </div>
                <div className='individualOptions'>                
                <img className='optionsImage' src={require('./Images/TopRated.webp')} />
                <h4 className='ourPicksOption'>TOP RATED</h4>
                </div>
                <div className='individualOptions'>                
                <img className='optionsImage' src={require('./Images/SuperFastDelivery.webp')} />
                <h4 className='ourPicksOption'>SUPER FAST DELIVERY</h4>
                </div>
                <div className='individualOptions'>                
                <img className='optionsImage' src={require('./Images/Biriyani.webp')} />
                <h4 className='ourPicksOption'>BIRIYANI</h4>
                </div>
                <div className='individualOptions'>                
                <img className='optionsImage' src={require('./Images/NorthIndian.webp')} />
                <h4 className='ourPicksOption'>NORTH INDIAN</h4>
                </div>
                <div className='individualOptions'>                
                <img className='optionsImage' src={require('./Images/SouthIndian.webp')} />
                <h4 className='ourPicksOption'>SOUTH INDIAN</h4>
                </div>

             </div>

        </div>
     
    )
  }
}

export default HomeOurPicksForYou