import React, { Component } from 'react'
import "./styles.css"

class SearchBarInHome extends Component {
    render() {
        return (
            <div className='searchBarHomePage'>
                <div className='searchBarContainer'>

                    <h4 className='searchTitle'>Great restaurants in Bangalore, delivering to you</h4>
                    <p className='searchText '>Set exact location to find the right restaurants near you.</p>
                    <div class="input-group">
                        <input type="search" class="form-control " placeholder="Enter street name, area etc..." aria-label="Search" aria-describedby="search-addon" />
                        <button type="button" class="btn-default homeSearchBar">FIND FOOD</button>
                    </div>


                </div>



            </div>
        )
    }
}

export default SearchBarInHome